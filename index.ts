import {Cup} from "./Classes/Cup";
import {Player} from "./Classes/Player";
import {Game} from "./Classes/Game";

const player1 : Player = new Player('Benjamin');
const player2 : Player = new Player('Romain');
const player3 : Player = new Player('Arthur');

//const cup : Cup = new Cup();

//cup.lancer(player1, player2);

//console.log(de.valeur);

//cup.afficherScore();

const game = new Game();

game.initialiserPartie(player1, player2, player3);
game.afficherGagnant();